/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],

  theme: {
    /*---Start Addapter to bootstrap breakpoints---*/
    container: {
      center: true,
      /*
      padding: {
        DEFAULT: "1rem",
        sm: "calc(18px + 1rem)",
        md: "calc(24px + 1rem)",
        lg: "calc(16px + 1rem)",
        xl: "calc(30px + 1rem)",
        "2xl": "calc(40px + 1rem)",
      },
      */
      /*
      screens: {
        sm: "576px",
        md: "768px",
        lg: "992px",
        xl: "1200px",
        "2xl": "1400px",
      },
      */
    },
    /*---End Addapter to bootstrap breakpoints---*/
    extend: {
      colors: {
        "body-s": "hsl(214, 47%, 23%)",
        "body-e": "hsl(237, 49%, 15%)",
        "scissors-s": "hsl(39, 89%, 49%)",
        "scissors-e": "hsl(40, 84%, 53%)",
        "paper-s": "hsl(230, 89%, 62%)",
        "paper-e": "hsl(230, 89%, 65%)",
        "rock-s": "hsl(349, 71%, 52%)",
        "rock-e": "hsl(349, 70%, 56%)",
        "lizard-s": "hsl(261, 73%, 60%)",
        "lizard-e": "hsl(261, 72%, 63%)",
        "cyan-s": "hsl(189, 59%, 53%)",
        "cyan-e": "hsl(189, 58%, 57%)",
        "dark-text": "hsl(229, 25%, 31%)",
        "score-text": "hsl(229, 64%, 46%)",
        "header-outline": "hsl(217, 16%, 45%)",
      },
      fontFamily: {
        roboto: ["var(--font-roboto)"],
        barlow: ["var(--font-barlow)"],
      },
    },
  },

  plugins: [],
};
