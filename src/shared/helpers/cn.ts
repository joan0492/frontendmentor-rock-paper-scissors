/**
 *  src/shared/helpers/cn.ts
 *  Install packages:
 *  npm i clsx
 *  npm i tailwind-merge
 */
import { clsx, type ClassValue } from "clsx";
import { twMerge } from "tailwind-merge";

export default function cn(...args: ClassValue[]) {
  return twMerge(clsx(args));
}
