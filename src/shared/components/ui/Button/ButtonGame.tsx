import cn from "@/shared/helpers/cn";

interface IButton extends React.ComponentPropsWithoutRef<"button"> {}

interface IProps extends IButton {
  img: string;
  imgAlt: string;
}

export const ButtonGame = ({
  img,
  imgAlt,
  className,
  ...restProps
}: IProps) => {
  return (
    <div
      className={cn(
        "bg-gradient-to-r w-[132px] h-[132px] rounded-full flex items-center justify-center transition-all duration-300",
        "hover:opacity-65",
        className
      )}
    >
      <button
        className={cn(
          "bg-white w-[113px] h-[113px] rounded-full flex justify-center items-center"
        )}
        {...restProps}
      >
        <img src={img} alt={imgAlt} />
      </button>
    </div>
  );
};
