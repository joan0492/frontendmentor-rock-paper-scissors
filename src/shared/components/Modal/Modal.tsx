interface IProps {
  closeModal: () => void;
}
export default function Modal({ closeModal }: IProps) {
  return (
    <div className="fixed inset-0 bg-white">
      <div className="pt-[100px] px-8 text-center relative">
        <p className="text-[1.94rem]/none">RULES</p>
        <div className="mt-24">
          <img src="/image-rules.svg" alt="Rules" className="mx-auto" />
        </div>
        <div className="mt-28">
          <button onClick={closeModal}>
            <img src="/icon-close.svg" alt="Icon Close" />
          </button>
        </div>
      </div>
    </div>
  );
}
