import { dataGameButtons } from "@/data";
import { ButtonGame } from "../ui";
import { picks } from "@/domain";

interface IProps {
  handleMyPick: (pick: picks) => void;
}
export const ScreenPicked = ({ handleMyPick }: IProps) => {
  return (
    <div className="mt-20 flex flex-wrap justify-between gap-4 bg-[url('/bg-triangle.svg')] bg-[15px_50px] bg-no-repeat">
      {dataGameButtons.map(({ id, ...restProps }) => (
        <ButtonGame {...restProps} key={id} onClick={() => handleMyPick(id)} />
      ))}
    </div>
  );
};
