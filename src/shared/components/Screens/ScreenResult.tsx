import { allPicks, dataGameButtons, pickBeatsTo } from "@/data";
import { IgameButtons, picks } from "@/domain";
import { ButtonGame } from "../ui";
import cn from "@/shared/helpers/cn";
import { useEffect, useState } from "react";

interface IProps {
  myPick: picks;
}
export const ScreenResult = ({ myPick }: IProps) => {
  const [counter, setCounter] = useState<number>(3);
  const [btnRandom, setBtnRandom] = useState<IgameButtons | null>(null);
  // const message = "";

  const { className, ...props } = dataGameButtons.find(
    (btn) => btn.id === myPick
  )!;

  useEffect(() => {
    const interval = setInterval(() => {
      setCounter((prev) => prev - 1);
    }, 1000);

    setTimeout(() => {
      clearInterval(interval);
      const index = Math.floor(Math.random() * allPicks.length);
      const pickRandom = allPicks[index];
      setBtnRandom(dataGameButtons.find((btn) => btn.id === pickRandom)!);
    }, 3000);

    return () => clearInterval(interval);
  }, []);

  if (btnRandom !== null) {
    const pcPick = btnRandom.id;
    if (pcPick === myPick) {
      console.log("DRAW");
      return;
    }
    if (pickBeatsTo[myPick] === pcPick) {
      console.log("YOU WIN");
      return;
    } else {
      console.log("ummm");
    }
    console.log("YOU LOSE");
  }

  return (
    <div className="mt-20 flex justify-between items-center">
      <ButtonGame
        {...props}
        className={cn(className, "pointer-events-none select-none mx-0")}
      />

      {btnRandom === null ? (
        <div
          className={cn(
            "bg-white w-[113px] h-[113px] rounded-full flex justify-center items-center font-bold text-5xl"
          )}
        >
          {counter}
        </div>
      ) : (
        <ButtonGame
          {...btnRandom}
          className={cn(
            btnRandom.className,
            "pointer-events-none select-none mx-0"
          )}
        />
      )}
    </div>
  );
};
