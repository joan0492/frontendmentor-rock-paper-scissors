import { useState } from "react";
import Modal from "./shared/components/Modal/Modal";
import { ScreenPicked, ScreenResult } from "./shared/components/Screens";
import { picks } from "./domain";

export const App = () => {
  const [showModal, setShowModal] = useState<boolean>(false);
  const [score] = useState<number>(12);
  const [myPick, setMyPick] = useState<picks | null>(null);

  const handleCloseModal = () => setShowModal(false);
  // const handlerChangedScore = (amount: number) =>
  //   setScore((prev) => Math.max(prev + amount, 0));
  const handleMyPick = (pick: picks) => setMyPick(pick);

  return (
    <>
      <main className="min-h-screen ">
        <div className="container ">
          <div className="grid grid-cols-12 gap-4">
            <div className="col-span-12">
              <div className="pt-[30px] pb-14 w-full max-w-[343px] mx-auto">
                <header className="flex items-center justify-between border-2 border-header-outline rounded-lg py-[11px] pl-[22px] pr-[11px]">
                  <div className="max-w-20">
                    <img src="/logo.svg" alt="Logo App" className="w-full" />
                  </div>
                  <div className="h-[72px] bg-white text-center rounded-md pt-[9px] px-[1.475rem]">
                    <p className="text-score-text text-xs tracking-wide">
                      SCORE
                    </p>
                    <p className="text-4xl font-bold text-header-outline">
                      {score}
                    </p>
                  </div>
                </header>

                {myPick === null ? (
                  <ScreenPicked handleMyPick={handleMyPick} />
                ) : (
                  <ScreenResult myPick={myPick} />
                )}

                <div className="text-center mt-24">
                  <button
                    type="button"
                    className="bg-transparent transition-all duration-300  rounded-lg text-sm px-[2.95rem] py-[11px] text-white outline outline-2 outline-header-outline"
                    onClick={() => setShowModal(true)}
                  >
                    RULES
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
      {showModal && <Modal closeModal={handleCloseModal} />}
    </>
  );
};
