export type picks = "paper" | "scissors" | "rock";

export interface IgameButtons {
  id: picks;
  img: string;
  imgAlt: string;
  className: string;
}
