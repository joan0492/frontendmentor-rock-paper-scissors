import { IgameButtons, picks } from "@/domain";

export const allPicks: picks[] = ["paper", "scissors", "rock"];
export const pickBeatsTo: Record<picks, picks> = {
  paper: "rock",
  scissors: "paper",
  rock: "scissors",
};

export const dataGameButtons: IgameButtons[] = [
  {
    id: "paper",
    img: "/icon-paper.svg",
    imgAlt: "Paper",
    className: "from-paper-s to-paper-e",
  },
  {
    id: "scissors",
    img: "/icon-scissors.svg",
    imgAlt: "Scissors",
    className: "from-scissors-s to-scissors-e",
  },
  {
    id: "rock",
    img: "/icon-rock.svg",
    imgAlt: "Rock",
    className: "mx-auto from-rock-s to-rock-e",
  },
];
